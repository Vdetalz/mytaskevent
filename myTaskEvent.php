<?php
/*
Plugin Name: myTaskEvent
Description: плагин событий.
Version: 1.0
Author: Vitaliy
*/

/**
 * translation
 */
add_action( 'plugins_loaded', 'event_load_plugin_textdomain' );

function event_load_plugin_textdomain() {
	load_plugin_textdomain( 'vitaliy', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}

add_filter( 'locale', 'event_localize_theme' );

function event_localize_theme( $locale ) {
	if ( isset( $_GET['lang'] ) ) {
		return esc_attr( $_GET['lang'] );
	}

	return $locale;
}

/**
 * add new taxonomy
 */
add_action( 'init', 'my_events_taxonomy' );
function my_events_taxonomy() {
	$labels = array(
		'name'              => __( 'Events Categories', 'vitaliy' ),
		'singular_name'     => __( 'Event Category', 'vitaliy' ),
		'search_items'      => __( 'Search Category Events', 'vitaliy' ),
		'all_items'         => __( 'All', 'vitaliy' ),
		'parent_item'       => __( 'Parent Category', 'vitaliy' ),
		'parent_item_colon' => __( 'Parent Category:', 'vitaliy' ),
		'edit_item'         => __( 'Edit Category', 'vitaliy' ),
		'view item'         => __( 'View Category', 'vitaliy' ),
		'update_item'       => __( 'Update Category', 'vitaliy' ),
		'add_new_item'      => __( 'Add Category', 'vitaliy' ),
		'new_item_name'     => __( 'New Category Name', 'vitaliy' ),
		'menu_name'         => __( 'Events Categories', 'vitaliy' )
	);
	$args   = array(
		'label'             => __( 'Events Categories', 'vitaliy' ),
		'labels'            => $labels,
		'public'            => true,
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_in_menu'      => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array(
			'slug' => '' // Текст в ЧПУ. По умолчанию: название таксономии.
			/*'with_front' => false, // Позволяет ссылку добавить к базовому URL.
			'hierarchical' => true, // Использовать (true) или не использовать (false) древовидную структуру ссылок. По умолчанию: false.
			'ep_mask' => EP_NONE // Перезаписывает конечное значение таксономии. По умолчанию: EP_NONE.
		*/
		),
	);
	register_taxonomy( 'taxplace', array( 'events' ), $args );

}

/**
 * add new taxonomy
 */
add_action( 'init', 'my_meets_taxonomy' );
function my_meets_taxonomy() {
	$labels = array(
		'name'              => __( 'Meets Categories', 'vitaliy' ),
		'singular_name'     => __( 'Meets Category', 'vitaliy' ),
		'search_items'      => __( 'Search Category', 'vitaliy' ),
		'all_items'         => __( 'All', 'vitaliy' ),
		'parent_item'       => __( 'Parent Category', 'vitaliy' ),
		'parent_item_colon' => __( 'Parent Category:', 'vitaliy' ),
		'edit_item'         => __( 'Edit Category', 'vitaliy' ),
		'view item'         => __( 'View Category', 'vitaliy' ),
		'update_item'       => __( 'Update Category', 'vitaliy' ),
		'add_new_item'      => __( 'Add New', 'vitaliy' ),
		'new_item_name'     => __( 'New Name Category', 'vitaliy' ),
		'menu_name'         => __( 'Categories Meets', 'vitaliy' )
	);
	$args   = array(
		'label'             => __( 'Categories Meets', 'vitaliy' ),
		'labels'            => $labels,
		'public'            => true,
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_in_menu'      => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array(
			'slug' => '' // Текст в ЧПУ. По умолчанию: название таксономии.
			/*'with_front' => false, // Позволяет ссылку добавить к базовому URL.
			'hierarchical' => true, // Использовать (true) или не использовать (false) древовидную структуру ссылок. По умолчанию: false.
			'ep_mask' => EP_NONE // Перезаписывает конечное значение таксономии. По умолчанию: EP_NONE.
		*/
		),
	);
	register_taxonomy( 'taxmeets', array( 'meets' ), $args );

}

/**
 * add custom post
 */
add_action( 'init', 'register_my_post_events' );
function register_my_post_events() {

	$args = array(
		'label'        => __( 'Events ', 'vitaliy' ),
		'labels'       => array(
			'name'               => __( 'Events', 'vitaliy' ),
			// основное название для типа записи
			'singular_name'      => __( 'Event', 'vitaliy' ),
			// название для одной записи этого типа
			'add_new'            => __( 'Add Event', 'vitaliy' ),
			// для добавления новой записи
			'add_new_item'       => __( 'Add Title Event', 'vitaliy' ),
			// заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => __( 'Edit Event', 'vitaliy' ),
			// для редактирования типа записи
			'new_item'           => __( 'New Event', 'vitaliy' ),
			// текст новой записи
			'view_item'          => __( 'View Event', 'vitaliy' ),
			// для просмотра записи этого типа.
			'search_items'       => __( 'Search Event', 'vitaliy' ),
			// для поиска по этим типам записи
			'not_found'          => __( 'Not Found', 'vitaliy' ),
			// если в результате поиска ничего не было найдено
			'not_found_in_trash' => __( 'Not Found In Trash', 'vitaliy' )
			// если не было найдено в корзине
		),
		'public'       => true,
		'hierarchical' => false,
		'show_ui'      => true,
		'show_in_menu' => true,
		'supports'     => array( 'title', 'editor', 'author', 'thumbnail', 'comments' ),
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'   => array( 'taxplace' ),
		'has_archive'  => true,
		'rewrite'      => array( 'slug' => 'events' ),
		'query_var'    => true
	);
	register_post_type( 'events', $args );

}

/**
 * add custom post
 */
add_action( 'init', 'register_my_post_meets' );
function register_my_post_meets() {

	$args = array(
		'label'        => __( 'Meets', 'vitaliy' ),
		'labels'       => array(
			'name'               => __( 'Meets', 'vitaliy' ),
			// основное название для типа записи
			'singular_name'      => __( 'Meet', 'vitaliy' ),
			// название для одной записи этого типа
			'add_new'            => __( 'Add Meet', 'vitaliy' ),
			// для добавления новой записи
			'add_new_item'       => __( 'Add Title Meet', 'vitaliy' ),
			// заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => __( 'Edit Meet', 'vitaliy' ),
			// для редактирования типа записи
			'new_item'           => __( 'New Meet', 'vitaliy' ),
			// текст новой записи
			'view_item'          => __( 'View Meet', 'vitaliy' ),
			// для просмотра записи этого типа.
			'search_items'       => __( 'Search Meet', 'vitaliy' ),
			// для поиска по этим типам записи
			'not_found'          => __( 'Not Found', 'vitaliy' ),
			// если в результате поиска ничего не было найдено
			'not_found_in_trash' => __( 'Not Found In Trash', 'vitaliy' )
			// если не было найдено в корзине
		),
		'public'       => true,
		'hierarchical' => false,
		'show_ui'      => true,
		'show_in_menu' => true,
		'supports'     => array(
			'title',
			'editor',
			'author',
			'thumbnail',
			'comments',
			'revisions',
			'page-attributes',
			'post-formats'
		),
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'   => array( 'taxmeets' ),
		'has_archive'  => true,
		'rewrite'      => array( 'slug' => 'meets' ),
		'query_var'    => true
	);
	register_post_type( 'meets', $args );

}

/**
 * add metaboxes(custom fields type of events)
 */
add_action( 'add_meta_boxes', 'status_events_meta_boxes' );
function status_events_meta_boxes() {
	add_meta_box(
		'status',
		__( 'Status', 'vitaliy' ),
		'status_content',
		'events',
		'side',
		'high'
	);
}

function status_content( $post ) {
	$status = get_post_meta( $post->ID, '_status', true );
	//wp_nonce_field( 'my_action_events', 'my_events' );

	echo "<label><input type='radio' name='status' value='1'";
	checked( $status, 1 );
	echo "/>";
	_e( 'Open Event', 'vitaliy' );
	echo "</label><br />";
	echo "<label><input type='radio'  name='status' value='2'";
	checked( $status, 2 );
	echo "/>";
	_e( 'By Invitation Only', 'vitaliy' );
	echo "</label>";
}

/**
 * save metaboxes values
 */
add_action( 'save_post', 'status_save_meta_box' );
function status_save_meta_box( $post_id ) {
	$result = trim( strip_tags( $_POST['status'] ) );

	if ( ! isset( $result ) && get_post_type( $post_id ) != 'events' ) {
		return;
	}

	if ( wp_is_post_revision( $post_id ) ) {
		return;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( isset( $result ) ) {
		//check_admin_referer( 'my_action_events', 'my_events' );
		update_post_meta( $post_id, '_status', $result );
	}
}


/**
 * add metaboxes(custom fields date of events)
 */
add_action( 'add_meta_boxes', 'date_events_meta_boxes' );
function date_events_meta_boxes() {
	add_meta_box(
		'eventdate',
		__( 'Date', 'vitaliy' ),
		'date_content',
		'events',
		'side',
		'high'
	);
}

function date_content( $post ) {
	$event_date = get_post_meta( $post->ID, '_eventdate', true );
	//wp_nonce_field( 'my_action_date', 'my_date' );
	echo "<label>";
	_e( 'Choose The Date:', 'vitaliy' );
	echo "<input type='date' name='eventdate' value='";
	if ( ! empty( $event_date ) ) {
		echo $event_date;
	} else {
		echo date( 'd-m-Y' );
	}
	echo "'  /></label>";
}

/**
 * save metaboxes values
 */
add_action( 'save_post', 'date_save_meta_box' );
function date_save_meta_box( $post_id ) {
	$res = trim( strip_tags( $_POST['eventdate'] ) );

	if ( ! isset( $res ) ) {
		return;
	}

	if ( wp_is_post_revision( $post_id ) ) {
		return;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( isset( $_POST['eventdate'] ) ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		//check_admin_referer( 'my_action_date', 'my_date' );
		update_post_meta( $post_id, '_eventdate', $res );
	}
}

/**
 * add shortcode
 */
add_shortcode( 'hs', 'my_event_shortcode' );
function my_event_shortcode( $atts, $content = null ) {
	global $post;

	$atts = shortcode_atts( array(
		"hs_status"  => '1',
		"num_events" => '1'
	), $atts );

	if ( absint( $atts['hs_status'] ) > 2 ) {
		$atts['hs_status'] = '1';
	}

	$args_hs = array(
		'post_type'      => 'events',
		'posts_per_page' => sanitize_text_field( $atts['num_events'] ),
		'meta_query'     => array(
			'relation' => 'AND',
			array(
				'key'     => '_status',
				'value'   => trim( strip_tags( $atts['hs_status'] ) ),
				'compare' => '='
			),
			array(
				'key'     => '_eventdate',
				'value'   => date( 'Y-m-d' ),
				'compare' => '>='

			)
		),
		'meta_key'       => '_eventdate',
		'orderby'        => 'meta_value'
	);

	$hs_events = new WP_Query( $args_hs );
	ob_start();
	while ( $hs_events->have_posts() ): $hs_events->the_post();
		$hs_event_date   = get_post_meta( $post->ID, '_eventdate', true );
		$hs_event_status = get_post_meta( $post->ID, '_status', true );
		?>
		<p><a href="<?php the_permalink(); ?>"
		      rel="bookmark"
		      title="<?php the_title_attribute(); ?>Event information">
				<?php the_title(); ?>
			</a>
		</p>
		<?php echo '<p>' . __( 'Date: ', 'vitaliy' ) . $hs_event_date . '</p>';
		echo '<p>' . __( 'Type of event: ', 'vitaliy' ) . $hs_event_status . '</p>';

		//echo '<p>num_event: ' . esc_html( $num_event ) . '</p>';
		//echo '<p>status for display: ' . esc_html( $wid_status ) . '</p>';
	endwhile;
	wp_reset_postdata();

	$output_string = ob_get_contents();
	ob_end_clean();

	return $output_string;
}

/**
 * add widget - "my_event_widget"
 */
add_action( 'widgets_init', 'my_event_register_widgets' );
function my_event_register_widgets() {
	register_widget( 'my_event_widget' );
}

class my_event_widget extends WP_Widget {
	public function __construct() {
		$widget_ops = array(
			'classname'   => 'event_widget_class',
			'description' => __( 'Widget For Displaying Upcoming Events', 'vitaliy' )
		);
		$this->WP_Widget( 'my_event_widget', __( 'Event Widget', 'vitaliy' ), $widget_ops );
	}

	function form( $instance ) {
		$defaults = array(
			'title'      => __( 'Last Events', 'vitaliy' ),
			'num_event'  => '5',
			'wid_status' => '1'
		);

		$instance   = wp_parse_args( ( array ) $instance, $defaults );
		$title      = $instance['title'];
		$num_event  = $instance['num_event'];
		$wid_status = $instance['wid_status'];
		?>
		<p><?php _e( 'Title:', 'vitaliy' ); ?>
			<input class="widefat"
			       name="<?php echo $this->get_field_name( 'title' ); ?>"
			       type="text" value="<?php echo esc_attr_e( $title, 'vitaliy' ); ?>"/></p>
		<p><?php _e( 'Count Events:', 'vitaliy' ) ?>
			<input class="widefat" name="<?php echo $this->get_field_name( 'num_event' ); ?>"
			       type="text" value="<?php echo esc_attr( $num_event ); ?>"/>
		</p>
		<p><?php _e( 'View:', 'vitaliy' ); ?>
		<select class="widefat" name="<?php echo $this->get_field_name( 'wid_status' ); ?>">
			<option value="1" <?php selected( $wid_status, 1 ); ?> ><?php _e( 'Open Events', 'vitaliy' ); ?></option>
			<option
				value="2" <?php selected( $wid_status, 2 ); ?> ><?php _e( 'By Invitation Only', 'vitaliy' ); ?></option>
		</select>
		<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance               = $old_instance;
		$instance['title']      = sanitize_text_field( $new_instance['title'] );
		$instance['num_event']  = trim( strip_tags( $new_instance['num_event'] ) );
		$instance['wid_status'] = trim( strip_tags( $new_instance['wid_status'] ) );

		return $instance;
	}

	function widget( $args, $instance ) {
		global $post;

		extract( $args );
		echo $before_widget;
		$title      = apply_filters( 'widget_title', $instance['title'] );
		$num_event  = ( empty( $instance['num_event'] ) ) ? '' : $instance['num_event'];
		$wid_status = ( empty( $instance['wid_status'] ) ) ? '' : $instance['wid_status'];

		if ( ! empty( $title ) ) {
			echo $before_title . esc_html_e( $title, 'vitaliy' ) . $after_title;
		}

		$args = array(
			'post_type'      => 'events',
			'posts_per_page' => absint( $num_event ),
			'meta_query'     => array(
				'relation' => 'AND',
				array(
					'key'     => '_status',
					'value'   => trim( strip_tags( $wid_status ) ),
					'compare' => '='
				),
				array(
					'key'     => '_eventdate',
					'value'   => date( 'Y-m-d' ),
					'compare' => '>='

				),
			),
			'meta_key'       => '_eventdate',
			'orderby'        => 'meta_value'
		);

		$new_events = new WP_Query( $args );

		if( $new_events->have_posts() ) {
          while ($new_events->have_posts()): $new_events->the_post();
            $event_date   = get_post_meta($post->ID, '_eventdate', TRUE);
            $event_status = get_post_meta($post->ID, '_status', TRUE);
            ?>
            <p><a href="<?php the_permalink(); ?>"
                  rel="bookmark"
                  title="<?php the_title_attribute(); ?>">
                <?php the_title(); ?>
              </a>
            </p>
            <?php echo '<p>';
            _e('Date: ', 'vitaliy');
            echo $event_date . '</p>';
            echo '<p>';
            _e('Type Of Event: ', 'vitaliy');
            if ($event_status == 1) {
              _e('Open', 'vitaliy');
              echo '</p><hr />';
            }
            else {
              _e('Closed', 'vitaliy');
              echo '</p><hr />';
            }
          endwhile;
          wp_reset_postdata();
        } else {
            echo '<p>'; _e( 'Not Found' ); echo '<p>';
        }
		echo $after_widget;
		echo '<br/>';
	}
}